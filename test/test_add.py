from src.add import add

def test_add():
    assert add(1,1) == 2

def test_add_f():
    assert add(1,1) == 3

def test_add_long():
    assert add(100,100) == 200

